 $(document).ready(function(){
  $( ".flex-col figure  img" ).on('mouseenter tap',function() {
   	//make drawer visible and deactivate the rest of the elements
  	$( '#drawer_wrapper' ).addClass( 'show');
    $( '#drawer' ).addClass( 'show');
    $( '.disable' ).addClass( 'show');
    //find item's position inside grid
  	var index0 = $( ".flex-col figure  img" ).index( this );
  	var index = index0 + 1;
  	//load item's full image depended on its position
  	$('#drawer img').attr("src", "img/800x500/property"+index+"-w800.jpg");
  });
   
  //on document click remove drawer and reactivate all elements
  $(document).on('click', function () {
	if (!$(event.target).closest('#drawer').length) {
	   	$( '#drawer_wrapper' ).removeClass( 'show');
	    $( '#drawer' ).removeClass( 'show');
	    $( '.disable' ).removeClass( 'show');
	  }
	});

  $('#drawer a').on('click', function () {
	$( '#drawer_wrapper' ).removeClass( 'show');
    $( '#drawer' ).removeClass( 'show');
    $( '.disable' ).removeClass( 'show');
  });

  $('.flex-col .columns').hover(function() {
    $(this).children().addClass( 'show');
  },function(){
  	$(this).children().removeClass( 'show');
  }); 
});